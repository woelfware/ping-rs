build:
	cargo build

release:
	RUSTFLAGS='-C link-arg=-s' cargo build --release
