use std::process::Command;
use std::sync::{Arc, Mutex};
use std::thread;

fn ping<'a>(servers: &'static [&'a str]) -> Vec<String> {
    let mut threads = vec![];
    let results = Arc::new(Mutex::new(Vec::new()));

    for server in servers {
        let mut results = results.clone();

        threads.push(thread::spawn(move || {
            let output = Command::new("ping")
                .arg("-n")
                .arg("1")
                .arg(server)
                .output()
                .expect("failed to execute ping");
            let status = if output.status.success() {
                "alive"
            } else {
                "dead"
            };
            let result = format!("{}: {}", server, status);
            results.lock().unwrap().push(result);
        }));
    }

    for t in threads {
        let _ = t.join();
    }

    Arc::try_unwrap(results).unwrap().into_inner().unwrap()
}

fn main() {
    static SERVERS: &'static [&'static str] = &[
        "google.com",
        "yahoo.com",
        "fakeurl.rable",
    ];

    let results = ping(&SERVERS);

    for result in &results {
        println!("{}", result);
    }
}
